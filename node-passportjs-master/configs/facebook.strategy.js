const passport = require('passport');
const GitHubStrategy = require('passport-facebook').Strategy;

passport.use(new GitHubStrategy({
    clientID: '889562374716156',
    clientSecret: 'efda71e31f58a4000e78b1a8dde54e2c',
    callbackURL: "http://localhost:3000/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    return cb(undefined, profile);
  }
));

