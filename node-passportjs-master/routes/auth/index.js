const express         = require('express');
const router          = express.Router();
const githubRouter    = require('./github');
const facebookbRouter = require('./facebook');


router.use('/github', githubRouter);
router.use('/facebook', facebookbRouter);

module.exports = router