const express   = require('express')
const router    = express.Router();
const passsport =  require('passport');

router.get('/', passsport.authenticate('facebook'));    

router.get('/callback',
passsport.authenticate('facebook', {failureRedirect: '/login'}),
(req, res) => {
    res.redirect('/profile')
})

module.exports = router