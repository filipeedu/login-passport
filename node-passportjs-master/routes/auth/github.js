const express   = require('express')
const router    = express.Router();
const passsport =  require('passport');

router.get('/', passsport.authenticate('github'));

// como a aplicação sera autenticada 
router.get('/callback',
passsport.authenticate('github', {failureRedirect: '/login'}),
(req, res) => {
    res.redirect('/profile')
})

module.exports = router