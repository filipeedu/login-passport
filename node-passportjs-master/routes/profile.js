const express   = require('express')
const router    = express.Router();
router.get('/', 
    require('connect-ensure-login').ensureLoggedIn(), //retorna se tem usuário logado ou não bool
    (req,res) => {
        res.render('profile', {profile: req.user}) // retorna os campos relacionados ao perfil do usuário         
    })
module.exports = router